#pragma once
#include "../core/ISubsystem.h"

namespace CE {
	namespace core {
		class ImportManager : public ISubsystem
		{
		private:
			
		public:
			ImportManager();
			~ImportManager();

			bool Initialize();
			bool Shutdown();
		private:
		};
	}
}