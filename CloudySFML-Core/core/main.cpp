#include "Core.h"
#include <iostream>

int main(int argc, const char* argv[])
{
	CE::core::Core* CloudyEngine = new CE::core::Core();

	if (CloudyEngine->Initialize())
	{
		CloudyEngine->Run();
	}

	printf("%s\n", CloudyEngine->Shutdown() ? "true" : "false");

	delete CloudyEngine;
	CloudyEngine = nullptr;
	system("Pause");
	return 0;
}