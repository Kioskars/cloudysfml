#pragma once

#include "ISubsystem.h"	

#include "../graphics/GraphicsCore.h"
#include "../input/InputManager.h"
#include "../entitites/EntityManager.h"
#include "../physics/PhysicsCore.h"
#include "../states/GameStatesMachine.h"
#include "../audio/AudioManager.h"
#include "ImportManager.h"

#include "../states/PlayState.h"
#include <iostream>
#include <SFML\System.hpp>



namespace CE{ 
	namespace core {
		class Core
		{
		friend class Testing;
		private:			
			std::map<int, ISubsystem*> m_apxSubsystemList;
			
			CE::graphics::GraphicsCore* m_pxGraphicsCore;
			CE::input::InputManager* m_pxInputManager;
			CE::entities::EntityManager* m_pxEntityManager;
			CE::physics::PhysicsCore* m_pxPhysicsEngine;
			CE::states::GameStatesMachine* m_pxGameStatesMachine;
			CE::audio::AudioManager* m_pxAudioManager;
			CE::core::ImportManager* m_pxImportManager;

		public:
			Core();
			~Core();

			static std::string SfStringToSTDString(sf::String& p_sfString);

			void PrintSubsystemListInfo();

			

			bool Initialize();
			void Run();
			bool Shutdown();

			entities::EntityManager* GetEntityManager() { return m_pxEntityManager; }
			graphics::GraphicsCore* GetGraphicsCore() { return m_pxGraphicsCore; }
		
		private:
			void AllocateSubsystem(ISubsystem* p_Subsystem);
			void DeallocateSubsystem(ISubsystem* p_Subsystem);
		};
	} 
}