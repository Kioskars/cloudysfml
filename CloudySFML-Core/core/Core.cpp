#include "Core.h"
#include "System.h"

CE::core::Core::Core()
{
	printf("Created Core\n");
	m_pxGraphicsCore = nullptr;
	m_pxInputManager = nullptr;
	m_pxEntityManager = nullptr;
	m_pxPhysicsEngine = nullptr;
	m_pxGameStatesMachine = nullptr;
	m_pxAudioManager = nullptr;
	m_pxImportManager = nullptr;
}

CE::core::Core::~Core()
{	
	printf("Deleted Core\n");
}

std::string CE::core::Core::SfStringToSTDString(sf::String& p_sfString)
{
	std::string xConvertedString = p_sfString;
	return xConvertedString;
}

void CE::core::Core::PrintSubsystemListInfo()
{
	printf("Subsystem\t\tID\t\tAdress\n");
	for (std::map<int, ISubsystem*>::iterator it = m_apxSubsystemList.begin(); it != m_apxSubsystemList.end(); ++it)
	{
		printf("%s\t\t%i\t\t%p\n", it->second->GetSubsystemName().c_str(), it->second->GetSubsystemID(), it->second);
	}
}


bool CE::core::Core::Initialize()
{
	m_pxGraphicsCore = new graphics::GraphicsCore();
	printf("Created %s, %p\n", m_pxGraphicsCore->GetSubsystemName().c_str(), m_pxGraphicsCore);
	AllocateSubsystem(m_pxGraphicsCore);
	m_pxGraphicsCore->Initialize();

	m_pxInputManager = new input::InputManager();
	printf("Created %s, %p\n", m_pxInputManager->GetSubsystemName().c_str(), m_pxInputManager);
	AllocateSubsystem(m_pxInputManager);
	m_pxInputManager->Initialize();

	m_pxEntityManager = new entities::EntityManager();
	printf("Created %s, %p\n", m_pxEntityManager->GetSubsystemName().c_str(), m_pxEntityManager);
	AllocateSubsystem(m_pxEntityManager);
	m_pxEntityManager->Initialize();

	m_pxPhysicsEngine = new physics::PhysicsCore();
	printf("Created %s, %p\n", m_pxPhysicsEngine->GetSubsystemName().c_str(), m_pxPhysicsEngine);
	AllocateSubsystem(m_pxPhysicsEngine);
	m_pxPhysicsEngine->Initialize();

	m_pxGameStatesMachine = new states::GameStatesMachine();
	printf("Created %s, %p\n", m_pxGameStatesMachine->GetSubsystemName().c_str(), m_pxGameStatesMachine);
	AllocateSubsystem(m_pxGameStatesMachine);
	m_pxGameStatesMachine->Initialize();

	m_pxAudioManager = new audio::AudioManager();
	printf("Created %s, %p\n", m_pxAudioManager->GetSubsystemName().c_str(), m_pxAudioManager);
	AllocateSubsystem(m_pxAudioManager);
	m_pxAudioManager->Initialize();

	//Assign the initialized engine pointers to the globalsystem pointers
	GlobalSystem.m_pxSystemGraphicsCore = m_pxGraphicsCore;
	GlobalSystem.m_pxSystemAudioManager = m_pxAudioManager;
	GlobalSystem.m_pxSystemEntityManager = m_pxEntityManager;
	GlobalSystem.m_pxSystemGameStatesMachine = m_pxGameStatesMachine;
	GlobalSystem.m_pxSystemInputManager = m_pxInputManager;
	GlobalSystem.m_pxSystemPhysicsEngine = m_pxPhysicsEngine;
	
	m_pxImportManager = new core::ImportManager();
	printf("Created %s, %p\n", m_pxImportManager->GetSubsystemName().c_str(), m_pxImportManager);
	AllocateSubsystem(m_pxImportManager);
	m_pxImportManager->Initialize();

	m_pxGameStatesMachine->ChangeGameState(new CE::states::PlayState());

	return true;
}

void CE::core::Core::Run()
{
	while (m_pxGraphicsCore->GetWindow()->isOpen())
	{
		m_pxInputManager->HandleInput(*m_pxGraphicsCore->GetWindow());
		m_pxGameStatesMachine->CurrentGameState()->Update();
		m_pxGraphicsCore->Render();
		
		sf::sleep(sf::milliseconds(20));
	}

}

bool CE::core::Core::Shutdown()
{
	PrintSubsystemListInfo();
	ISubsystem* xSubsystemToDeallocate = m_apxSubsystemList.rbegin()->second;
	DeallocateSubsystem(xSubsystemToDeallocate);
	if (!m_apxSubsystemList.empty())
	{
		Shutdown();
	}
	return true;
}

void CE::core::Core::AllocateSubsystem(ISubsystem * p_Subsystem)
{
	m_apxSubsystemList.insert(std::pair<int, ISubsystem*>(p_Subsystem->GetSubsystemID(), p_Subsystem));
}

void CE::core::Core::DeallocateSubsystem(ISubsystem * p_Subsystem)
{
	int xKeyElement = p_Subsystem->GetSubsystemID();
	printf("Deallocating...%s, %i, %p\n", p_Subsystem->GetSubsystemName().c_str(), p_Subsystem->GetSubsystemID(), p_Subsystem);
	p_Subsystem->Shutdown();
	delete p_Subsystem;
	p_Subsystem = nullptr;
	m_apxSubsystemList.erase(xKeyElement);
}
