#pragma once
#include "../graphics/GraphicsCore.h"
#include "../input/InputManager.h"
#include "../entitites/EntityManager.h"
#include "../physics/PhysicsCore.h"
#include "../states/GameStatesMachine.h"
#include "../audio/AudioManager.h"
namespace CE {
	namespace core {
		struct System
		{
			graphics::GraphicsCore* m_pxSystemGraphicsCore;
			input::InputManager* m_pxSystemInputManager;
			entities::EntityManager* m_pxSystemEntityManager;
			physics::PhysicsCore* m_pxSystemPhysicsEngine;
			states::GameStatesMachine* m_pxSystemGameStatesMachine;
			audio::AudioManager* m_pxSystemAudioManager;
		 }; 
		extern struct System GlobalSystem;
	}
}
