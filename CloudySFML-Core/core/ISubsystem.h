#pragma once

#include <SFML\System.hpp>

enum SUBSYSTEMID
{
	SUBSYSTEMID_GRAPHICSENGINE = 1,
	SUBSYSTEMID_INPUTMANAGER = 2,
	SUBSYSTEMID_ENTITYMANAGER = 3,
	SUBSYSTEMID_PHYSICSENGINE = 4,
	SUBSYSTEMID_GAMESTATEMACHINE = 5,
	SUBSYSTEMID_AUDIOENGINE = 6,
	SUBSYSTEMID_IMPORTMANAGER = 7,
	SUBSYSTEMID_CREATIONMANAGER = 8
};

namespace CE {
	namespace core {
		class ISubsystem
		{
		public:
			virtual ~ISubsystem() {}
			virtual SUBSYSTEMID GetSubsystemID() { return m_iSubsystemID; }
			virtual std::string& GetSubsystemName() { return m_sSubsystemName; }
			virtual void SetSubsystemID(SUBSYSTEMID p_eID) { m_iSubsystemID = p_eID; }
			virtual void SetSubsystemName(std::string p_sName) { m_sSubsystemName = p_sName; }

			virtual bool Initialize() = 0;
			virtual bool Shutdown() = 0;

		private:
			SUBSYSTEMID m_iSubsystemID;
			std::string m_sSubsystemName;
		};
	}
}