#include "ImportManager.h"
#include "System.h"


CE::core::ImportManager::ImportManager()
{
	SetSubsystemID(SUBSYSTEMID_IMPORTMANAGER);
	SetSubsystemName("ImportManager");
	
	
}

CE::core::ImportManager::~ImportManager()
{
}

bool CE::core::ImportManager::Initialize()
{
		
	//Importing all graphics
	GlobalSystem.m_pxSystemGraphicsCore->GetSpriteManager()->CreateTexture("Sheet01", "assets/SpriteSheet.png", false, false);
	GlobalSystem.m_pxSystemGraphicsCore->GetTextManager()->ImportFont("../assets/fonts/arial.ttf");

	

	//Import all 

	return true;
}

bool CE::core::ImportManager::Shutdown()
{
	return false;
}
