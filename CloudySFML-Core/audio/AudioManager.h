#pragma once

#include "../core/ISubsystem.h"

namespace CE {
	namespace audio {
		class AudioManager : public CE::core::ISubsystem
		{
		private:
		public:
			AudioManager();
			~AudioManager();

			bool Initialize();
			bool Shutdown();
		private:
		};
	}
}