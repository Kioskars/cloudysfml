#pragma once
#include "../entitites/IEntity.h"
#include "../entitites/Helga.h"
#include "InputManager.h"


namespace CE {
	namespace input {
		class Command
		{
		public:
			virtual ~Command() {}
			virtual void Execute(CE::entities::Helga* p_pxEntity) = 0;

			
		};
		class MovementCommand : public Command
		{
		public: 
			
			virtual void Execute(CE::entities::Helga* p_pxEntity)
			{
				if (sf::Keyboard::isKeyPressed(p_pxEntity->GetKeyBoard().W))
					p_pxEntity->MoveUp();
				if (sf::Keyboard::isKeyPressed(p_pxEntity->GetKeyBoard().S))
					p_pxEntity->MoveDown();
				if (sf::Keyboard::isKeyPressed(p_pxEntity->GetKeyBoard().A))
					p_pxEntity->MoveLeft();
				if (sf::Keyboard::isKeyPressed(p_pxEntity->GetKeyBoard().D))
					p_pxEntity->MoveRight();
			}
		};
	}
}