#include "InputManager.h"

#include "../core/System.h"

CE::input::InputManager::InputManager()
{
	SetSubsystemID(SUBSYSTEMID_INPUTMANAGER);
	SetSubsystemName("InputManager");
}

CE::input::InputManager::~InputManager()
{
}

sf::Keyboard & CE::input::InputManager::GetKeyboard()
{
	return m_Keyboard;
}

void CE::input::InputManager::HandleInput(sf::RenderWindow & p_xWindow)
{
	sf::Event xEvent;

	while (p_xWindow.pollEvent(xEvent))
	{
		if (m_Keyboard.isKeyPressed(xEvent.key.code))
			m_EntityMovement.Execute(core::GlobalSystem.m_pxSystemEntityManager->GetHelgaEntity("HelgaEntity"));
		if (xEvent.type == sf::Event::Closed)
			p_xWindow.close();
	}
}

bool CE::input::InputManager::Initialize()
{
	
	return true;
}

bool CE::input::InputManager::Shutdown()
{
	return false;
}
