#pragma once
#include "../core/ISubsystem.h"


#include "Command.h"
#include <SFML\Graphics\RenderWindow.hpp>
#include <SFML\Window\Event.hpp>

#include <map>

namespace CE {
	namespace input {
		class InputManager : public CE::core::ISubsystem
		{
		private:			
			sf::Keyboard m_Keyboard;

			MovementCommand m_EntityMovement;
		public:
			InputManager();
			~InputManager();
			
			sf::Keyboard& GetKeyboard();
			void HandleInput(sf::RenderWindow& p_xWindow);

			bool Initialize();
			bool Shutdown();
		private:
			
		};
	}
}