#include "BoxCollider.h"

CE::physics::BoxCollider::BoxCollider()
{
}

CE::physics::BoxCollider::BoxCollider(sf::Vector2f p_Size, sf::Vector2f p_Position)
{
	setSize(p_Size);
	sf::RectangleShape::setPosition(p_Position);
}

CE::physics::BoxCollider::~BoxCollider()
{
	
}
