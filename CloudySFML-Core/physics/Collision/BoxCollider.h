#pragma once
#include <SFML\Graphics\RectangleShape.hpp>

namespace CE {
	namespace physics {
		class BoxCollider : public sf::RectangleShape
		{
		private:
		public:
			BoxCollider();
			BoxCollider(sf::Vector2f p_Size, sf::Vector2f p_Position);
			~BoxCollider();
		private:
		};
	}
}