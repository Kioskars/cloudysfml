#include "PhysicsCore.h"

CE::physics::PhysicsCore::PhysicsCore()
{
	SetSubsystemID(SUBSYSTEMID_PHYSICSENGINE);
	SetSubsystemName("PhysicsEngine");

}

CE::physics::PhysicsCore::~PhysicsCore()
{
}

bool CE::physics::PhysicsCore::Initialize()
{
	return false;
}

bool CE::physics::PhysicsCore::Shutdown()
{
	return false;
}

bool CE::physics::PhysicsCore::CheckBoundingBox(BoxCollider& p_ColliderOne, BoxCollider& p_ColliderTwo)
{
	/*if (p_EntityOne.GetSprite().getGlobalBounds().intersects(p_EntityTwo.GetSprite().getGlobalBounds()))
	{
		printf("%s collided with %s\n", p_EntityOne.GetEntityName().c_str(), p_EntityTwo.GetEntityName().c_str());
		return true;
	}
	else
		return false;*/

	if (p_ColliderOne.getGlobalBounds().intersects(p_ColliderTwo.getGlobalBounds()))
	{
		return true;
	}
	return false;
	

}
