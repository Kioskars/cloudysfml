#pragma once

#include "../core/ISubsystem.h"
#include "../entitites/IEntity.h"
#include "Collision\BoxCollider.h"

#include <vector>

namespace CE {
	namespace physics {
		class PhysicsCore : public CE::core::ISubsystem
		{
		private:
			
		public:
			PhysicsCore();
			~PhysicsCore();

			bool Initialize();
			bool Shutdown();
			bool CheckBoundingBox(BoxCollider& p_ColliderOne, BoxCollider& p_ColliderTwo);
		private:
			
			
		};
	}
}