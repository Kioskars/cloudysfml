#include "EntityManager.h"
#include "../core/System.h"

CE::entities::EntityManager::EntityManager()
{
	SetSubsystemID(SUBSYSTEMID_ENTITYMANAGER);
	SetSubsystemName("EntityManager");
}

CE::entities::EntityManager::~EntityManager()
{

}

void CE::entities::EntityManager::PrintEntitiesInfo()
{
	printf("EntityType\t\tEntityName\t\tAdress\n");
	for (std::vector<Helga*>::iterator it = m_apxHelgaEntities.begin(); it != m_apxHelgaEntities.end(); ++it)
	{
		printf("%u\t\t%s\t\t%p\n", (*it)->GetEntityType(), (*it)->GetEntityName().c_str(), (*it));
	}
}


void CE::entities::EntityManager::CreateEntity(EENTITYTYPE p_EntityType, std::string p_EntityName)
{
	switch (p_EntityType)
	{
	case ENTITY_PLAYER:
		CreateHelgaEntity(p_EntityName);
			break;
	case ENTITY_ENEMY:
		
		break;
	default:
		break;
	}
}

void CE::entities::EntityManager::DeleteEntity(std::string p_EntityName)
{
	Helga* xEntity = GetHelgaEntity(p_EntityName);
	printf("Delete...%s\n", xEntity->GetEntityName().c_str());
	//delete xEntity; //MinnesL�cka?????? Fast krash ifall enabled -.-
	xEntity->~Helga();
	xEntity = nullptr;
	m_apxHelgaEntities.pop_back();
	PrintEntitiesInfo();
}

void CE::entities::EntityManager::CreateHelgaEntity(std::string p_EntityName)
{
	//Skall detta Beh�llas??? 
	std::string xNewName = p_EntityName;
	if(!m_apxHelgaEntities.empty())
	{
		for (auto it = m_apxHelgaEntities.begin(); it != m_apxHelgaEntities.end(); ++it)
		{
			if ((*it)->GetEntityName() == p_EntityName)
			{
				int number = m_apxHelgaEntities.size();
				 xNewName = p_EntityName + std::to_string(number);
			}
		}
	}
		
	Helga* xHelga = new Helga(xNewName);
	xHelga->AssignKeyboard(core::GlobalSystem.m_pxSystemInputManager->GetKeyboard());
	m_apxHelgaEntities.push_back(xHelga);
	PrintEntitiesInfo();
}

CE::entities::Helga * CE::entities::EntityManager::GetHelgaEntity(std::string p_EntityName)
{
	auto it = m_apxHelgaEntities.rbegin();
	do
	{
		if ((*it)->GetEntityName() == p_EntityName)
		{
			return (*it);
		}
		else
		{
			++it;
		}
	} while (it != m_apxHelgaEntities.rend());
	printf("Didn't find any Entity with that name.\n");
	return nullptr;
}


bool CE::entities::EntityManager::Initialize()
{
	return true;
}

bool CE::entities::EntityManager::Shutdown()
{
	while (!m_apxHelgaEntities.empty())
	{
		PrintEntitiesInfo();
		Helga* xEntity = m_apxHelgaEntities.back();
		printf("%s, %i, %p\n", xEntity->GetEntityName().c_str(), xEntity->GetEntityType(), xEntity);
		DeleteEntity(xEntity->GetEntityName());
	}
	return true;
}
