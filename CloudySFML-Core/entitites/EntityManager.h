#pragma once

#include "../core/ISubsystem.h"
#include "IEntity.h"
#include "Helga.h"

#include <algorithm>
#include <vector>


namespace CE {
	namespace entities {
		class EntityManager : public CE::core::ISubsystem
		{
		private:
			std::vector <IEntity*> m_apxEntities;
			std::vector <Helga*> m_apxHelgaEntities;
			
		public:
			EntityManager();
			~EntityManager();

			void PrintEntitiesInfo();

			void CreateEntity(EENTITYTYPE p_EntityType, std::string p_EntityName);
			void DeleteEntity(std::string p_EntityName);
			
			void CreateHelgaEntity(std::string p_EntityName);
			Helga* GetHelgaEntity(std::string p_EntityName);

			bool Initialize();
			bool Shutdown();
		private:
		};
	}
}