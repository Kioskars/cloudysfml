#pragma once
#include <SFML\Graphics.hpp>

#include "../physics/Collision/BoxCollider.h"

#include <string>

enum EENTITYTYPE
{
	ENTITY_PLAYER,
	ENTITY_ENEMY,
	ENTIY_BOSS
};

namespace CE {
	namespace entities{
		class IEntity : public sf::Drawable, public sf::Transformable
		{
		protected:
			std::string m_sEntityName;
			EENTITYTYPE m_EntityType;

			sf::VertexArray m_Vertices = sf::VertexArray(sf::Quads, 4);
			sf::Sprite m_Sprite;
			sf::Vector2f m_vPosition;
			std::vector<unsigned int> m_uPixelsInformation;
			
			bool m_IsColliding;
		public:
			IEntity() {}
			virtual ~IEntity() {}
			virtual std::string GetEntityName() { return m_sEntityName; }
			virtual EENTITYTYPE GetEntityType() { return m_EntityType; }

			virtual sf::Sprite& GetSprite() { return m_Sprite; }
			virtual void SetSprite(sf::Sprite p_pxSprite) 
			{ m_Sprite = p_pxSprite; 
			
			//Give the vertices the same positions as the sprites dimension. Needs refactoring...

			//Size of Texture Rect
			m_Sprite.setPosition(m_vPosition);

			sf::Vector2f xTextureRectSize = sf::Vector2f(m_Sprite.getTextureRect().width, m_Sprite.getTextureRect().height);
			sf::Vector2f xTextureRectPosition = sf::Vector2f(m_Sprite.getTextureRect().left, m_Sprite.getTextureRect().top);

			m_Vertices[0].position = xTextureRectSize - xTextureRectSize;
			m_Vertices[1].position = sf::Vector2f(xTextureRectSize.x, xTextureRectSize.y - xTextureRectSize.y);
			m_Vertices[2].position = xTextureRectSize;
			m_Vertices[3].position = sf::Vector2f(xTextureRectSize.x - xTextureRectSize.x, xTextureRectSize.y);

			//Position of Texture rect 
			m_Vertices[0].texCoords = xTextureRectPosition;
			m_Vertices[1].texCoords = sf::Vector2f(xTextureRectPosition.x + xTextureRectSize.x, xTextureRectPosition.y);
			m_Vertices[2].texCoords = xTextureRectPosition + xTextureRectSize;
			m_Vertices[3].texCoords = sf::Vector2f(xTextureRectPosition.x, xTextureRectPosition.y + xTextureRectSize.y);

			}

			virtual void SetPixelsInformation(std::vector<unsigned int> p_uPixelsInformation) { m_uPixelsInformation = p_uPixelsInformation; }
			virtual std::vector<unsigned int>& GetPixelsInformation() { return m_uPixelsInformation; }

			virtual sf::VertexArray& GetVertexArray() { return m_Vertices; }

			virtual bool GetCollidedInfo() { return m_IsColliding; }
			virtual void SetCollided(bool p_Collide) { m_IsColliding = p_Collide; }

			virtual void SetPosition(sf::Vector2f p_Pos) { m_vPosition = p_Pos; setPosition(m_vPosition); }
		protected:
			virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;

			
		};
	}
}