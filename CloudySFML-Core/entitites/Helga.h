#pragma once

#include "IEntity.h"
#include <SFML\Window\Keyboard.hpp>


namespace CE {
	namespace entities {
		class Helga : public IEntity
		{
		private:
			EENTITYTYPE m_EntityType;
			float m_fSpeed;
			int m_iHealthpoints;
			int m_iLongAmmunition;

			sf::Keyboard m_Keyboard;
			physics::BoxCollider m_Collider;
		public:
			Helga(std::string p_sEntityName);
			~Helga();

			void AssignKeyboard(sf::Keyboard& p_Keyboard);
			sf::Keyboard& GetKeyBoard();

			void Update();
			
			void draw(sf::RenderTarget& target, sf::RenderStates states) const;
			void Attack();

			physics::BoxCollider& GetCollider();

			void MoveUp();
			void MoveDown();
			void MoveLeft();
			void MoveRight();
		private:
			void CloseRangeAttack();
			void LongRangeAttack();
			
			
		};
	}
}