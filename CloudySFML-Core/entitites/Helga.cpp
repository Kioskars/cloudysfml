#include "Helga.h"

CE::entities::Helga::Helga(std::string p_sEntityName)
{
	m_EntityType = ENTITY_PLAYER;
	m_sEntityName = p_sEntityName;
	
	m_fSpeed = 3.0f;

	m_Collider = physics::BoxCollider(sf::Vector2f(20, 12), sf::Vector2f(m_vPosition.x + 20, m_vPosition.y + 52));
}

CE::entities::Helga::~Helga()
{
	printf("Deleted %s, %p\n", this->GetEntityName().c_str(), this);
}

void CE::entities::Helga::AssignKeyboard(sf::Keyboard & p_Keyboard)
{
	m_Keyboard = p_Keyboard;
}

sf::Keyboard & CE::entities::Helga::GetKeyBoard()
{
	return m_Keyboard;
}

void CE::entities::Helga::Update()
{
	setPosition(m_vPosition);
	m_Sprite.setPosition(m_vPosition);
	m_Collider.setPosition(sf::Vector2f(getPosition().x + 15, getPosition().y + 52));
	
}

void CE::entities::Helga::MoveUp()
{
		m_vPosition.y -= m_fSpeed;
}

void CE::entities::Helga::MoveDown()
{
		m_vPosition.y += m_fSpeed;
}

void CE::entities::Helga::MoveLeft()
{
		m_vPosition.x -= m_fSpeed;
}

void CE::entities::Helga::MoveRight()
{
		m_vPosition.x += m_fSpeed;
}

void CE::entities::Helga::Attack()
{
	printf("%s Attacked, she used: ", m_sEntityName.c_str());
	CloseRangeAttack();
	LongRangeAttack();
}

CE::physics::BoxCollider & CE::entities::Helga::GetCollider()
{
	return m_Collider;
}

void CE::entities::Helga::CloseRangeAttack()
{
	printf("The axe to slay her enemy\n");
}

void CE::entities::Helga::LongRangeAttack()
{
	printf("The knife with a precision throw\n");
}

void CE::entities::Helga::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	states.texture = m_Sprite.getTexture();

	target.draw(m_Vertices, states);
}

