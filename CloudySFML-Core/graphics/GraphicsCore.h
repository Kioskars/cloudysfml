#pragma once
#include "../core/ISubsystem.h"
#include "SpriteManager.h"
#include "GUI\TextManager.h"
#include "GUI\Label.h"
#include "GUI\Widget.h"

#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>


#include "../entitites/IEntity.h"
#include "../entitites/Helga.h"

#include <map>
#include <vector>
#include <iostream>
namespace CE {
	namespace graphics {
		class GraphicsCore : public CE::core::ISubsystem
		{
		private:
			
			int m_iWidth, m_iHeight;
			sf::String m_sTitle;
			sf::Vector2f m_vScale;
			sf::RenderWindow* m_pxWindow;
			sf::View m_Camera;
			SpriteManager* m_pxSpriteManager;
			TextManager* m_pxTextManager;
			
			std::vector<sf::Drawable*> m_vVertices;
			
		public:
			GraphicsCore();
			~GraphicsCore();
			

			void SetResolution(int p_iWidth, int p_iHeight);
			sf::Vector2i GetResolution();
			sf::RenderWindow* GetWindow();

			graphics::SpriteManager* GetSpriteManager();
			graphics::TextManager* GetTextManager();

			bool Render();
			void Draw(sf::Drawable& p_2Dobjects);

			bool Initialize();
			bool Shutdown();

			void DebugDraw(sf::Drawable& p_2Dobjects, const sf::RenderStates &states);
		private:
			
		};
	}
}