#include "SpriteManager.h"

CE::graphics::SpriteManager::SpriteManager()
{
}

CE::graphics::SpriteManager::~SpriteManager()
{
	m_apxSprites.clear();
	m_apxTextures.clear();
}

void CE::graphics::SpriteManager::CreateSprite(std::string p_sSpriteName, std::string p_sTexture, int p_iX, int p_iY, int p_iW, int p_iH)
{
	sf::Sprite xSprite;
	xSprite.setTexture(GetTexture(p_sTexture));
	xSprite.setTextureRect(sf::IntRect(p_iX*64, p_iY*64, p_iW, p_iH));
	m_apxSprites.insert(std::pair<std::string, sf::Sprite>(p_sSpriteName, xSprite));
}

void CE::graphics::SpriteManager::CreateTexture(std::string p_sTextureName, std::string p_sFilepath, bool p_Smooth, bool p_Repeat)
{
	sf::Texture xTexture;
	if (!xTexture.loadFromFile(p_sFilepath))
	{
		printf("Error: No Texture\n");
	}
	xTexture.setSmooth(p_Smooth);
	xTexture.setRepeated(p_Repeat);

	m_apxTextures.insert(std::pair<std::string, sf::Texture>( p_sTextureName, xTexture));
}

sf::Texture & CE::graphics::SpriteManager::GetTexture(std::string p_sTexture)
{
	return m_apxTextures.find(p_sTexture)->second;
}

sf::Sprite CE::graphics::SpriteManager::GetSprite(std::string p_sSprite)
{
	return m_apxSprites.find(p_sSprite)->second;
}

std::vector<unsigned int> CE::graphics::SpriteManager::GetPixelsInformation(sf::Sprite& p_Sprite)
{
	sf::Image xImage = p_Sprite.getTexture()->copyToImage();

	std::vector<unsigned int> xSpritePixels;
	for (int i = p_Sprite.getTextureRect().top; i < p_Sprite.getTextureRect().top + p_Sprite.getTextureRect().height; i++) //Size of Sprite
	{
		for (int j = p_Sprite.getTextureRect().left; j < p_Sprite.getTextureRect().left + p_Sprite.getTextureRect().width; j++) //Position of Sprite
		{

			if (xImage.getPixel(j, i).a > 0)
			{
				printf("1");
				xSpritePixels.push_back(1);
			}
			else
			{
				printf("0");
				xSpritePixels.push_back(0);
			}
		}
		printf("\n");
	}
	return xSpritePixels;
}