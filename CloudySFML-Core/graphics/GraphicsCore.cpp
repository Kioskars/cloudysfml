#include "GraphicsCore.h"
#include "../core/System.h"

CE::graphics::GraphicsCore::GraphicsCore()
{
	SetSubsystemID(SUBSYSTEMID_GRAPHICSENGINE);
	SetSubsystemName("GraphicsEngine");

	m_iWidth = 640;
	m_iHeight = 480;
	m_sTitle = "Helga Game";
	m_vScale = sf::Vector2f(2, 2);
	m_Camera = sf::View(sf::FloatRect(0, 0, 640, 480));

	m_pxWindow = nullptr;
	m_pxSpriteManager = nullptr;
	m_pxTextManager = nullptr;
}

CE::graphics::GraphicsCore::~GraphicsCore()
{
}

void CE::graphics::GraphicsCore::SetResolution(int p_iWidth, int p_iHeight)
{
	m_iWidth = p_iWidth;
	m_iHeight = p_iHeight;
}

sf::Vector2i CE::graphics::GraphicsCore::GetResolution()
{
	return sf::Vector2i(m_iWidth, m_iHeight);
}

sf::RenderWindow* CE::graphics::GraphicsCore::GetWindow()
{
	return m_pxWindow;
}

CE::graphics::SpriteManager * CE::graphics::GraphicsCore::GetSpriteManager()
{
	return m_pxSpriteManager;
}

CE::graphics::TextManager * CE::graphics::GraphicsCore::GetTextManager()
{
	return m_pxTextManager;
}

bool CE::graphics::GraphicsCore::Render()
{
	m_pxWindow->clear();
	m_pxWindow->setView(m_Camera);
	for (auto it = m_vVertices.begin(); it != m_vVertices.end(); ++it)
	{
		m_pxWindow->draw(*(*it));
	}

	sf::VertexArray FX(sf::Quads, 4);

	FX[0].position = sf::Vector2f(0, 0);
	FX[1].position = sf::Vector2f(640, 0);
	FX[2].position = sf::Vector2f(640, 480);
	FX[3].position = sf::Vector2f(0, 480);

	/*FX[0].color = sf::Color::Blue + sf::Color::Green;
	FX[1].color = sf::Color::Red + sf::Color::Green;
	FX[2].color = sf::Color::Blue + sf::Color::Green;
	FX[3].color = sf::Color::Red + sf::Color::Green;*/

	m_pxWindow->draw(FX, sf::BlendMultiply);

	
	m_pxWindow->display();
	m_vVertices.clear();
	return true;
}

void CE::graphics::GraphicsCore::Draw(sf::Drawable& p_2Dobjects)
{
	m_vVertices.push_back(&p_2Dobjects);
}


bool CE::graphics::GraphicsCore::Initialize()
{
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	settings.Debug;
	settings.depthBits = 32;
	//m_Camera.zoom(.75);
	m_pxWindow = new sf::RenderWindow(sf::VideoMode(GetResolution().x, GetResolution().y), m_sTitle, sf::Style::Default, settings);
	m_pxWindow->setVerticalSyncEnabled(false);
	m_pxWindow->setFramerateLimit(60);

	m_pxSpriteManager = new SpriteManager();

	m_pxTextManager = new TextManager();

	

	return true;
}

bool CE::graphics::GraphicsCore::Shutdown()
{
	m_vVertices.clear();

	delete m_pxTextManager;
	m_pxTextManager = nullptr;

	delete m_pxSpriteManager;
	m_pxSpriteManager = nullptr;

	delete m_pxWindow;
	m_pxWindow = nullptr;
	return true;
}

