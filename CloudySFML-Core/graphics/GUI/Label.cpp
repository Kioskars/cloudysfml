#include "Label.h"

CE::graphics::GUI::Label::Label(std::string p_FontName, unsigned int p_Size, sf::Color p_Color, sf::Vector2f p_Position)
{
	m_WidgetPos = p_Position;
	m_Text.setPosition(m_WidgetPos);
	m_Text.setFont(TextManager::GetFont(p_FontName));
	m_Text.setCharacterSize(p_Size);
	m_Text.setColor(p_Color);
}

CE::graphics::GUI::Label::~Label()
{
}

void CE::graphics::GUI::Label::SetText(std::string p_Text)
{
	m_Text.setString(p_Text);
}

void CE::graphics::GUI::Label::SetFont(std::string p_FontName)
{

	m_Text.setFont(TextManager::GetFont(p_FontName));
}

void CE::graphics::GUI::Label::SetSize(unsigned int p_Size)
{
	m_Text.setCharacterSize(p_Size);
}

void CE::graphics::GUI::Label::SetColor(sf::Color p_Color)
{
	m_Text.setColor(p_Color);
}

sf::Text CE::graphics::GUI::Label::GetText() const
{
	return m_Text;
}

sf::Font CE::graphics::GUI::Label::GetFont() 
{
	return *m_Text.getFont();
}

unsigned int CE::graphics::GUI::Label::GetSize() 
{
	return m_Text.getCharacterSize();
}

sf::Color CE::graphics::GUI::Label::GetColor()
{
	return m_Text.getColor();
}

void CE::graphics::GUI::Label::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(m_Text, states);
}

void CE::graphics::GUI::Label::Update()
{
	m_Text.setPosition(m_WidgetPos);
}
