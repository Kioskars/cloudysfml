#pragma once

#include "Widget.h"


namespace CE {
	namespace graphics {
		namespace GUI {
			class Label : public Widget
			{
			private:
				sf::Text m_Text;
			public:
				Label(std::string p_FontName, unsigned int p_Size, sf::Color p_Color, sf::Vector2f p_Position);
				~Label();

				void SetText(std::string p_Text);
				void SetFont(std::string p_FontName);
				void SetSize(unsigned int p_Size);
				void SetColor(sf::Color p_Color);

				sf::Text GetText() const;
				sf::Font GetFont();
				unsigned int GetSize();
				sf::Color GetColor();
				void Update();
			private:
				void draw(sf::RenderTarget& target, sf::RenderStates states) const;
				
			};
		}
	}
}