#pragma once

#include <SFML\Graphics\Drawable.hpp>
#include <SFML\Graphics\Sprite.hpp>
#include <SFML\Graphics\Transformable.hpp>
#include <SFML\Graphics\RenderTarget.hpp>

#include "TextManager.h"

namespace CE {
	namespace graphics {
		namespace GUI {
			class Widget : public sf::Drawable, protected TextManager
			{
			protected:
				sf::Sprite m_Sprite;
				sf::Vector2f m_WidgetPos;
			public:
				
				virtual ~Widget() {}
				
				virtual void SetWidgetPosition(sf::Vector2f p_Position) { m_WidgetPos = p_Position; }
				virtual void SetWidgetSprite(sf::Sprite p_Sprite) { m_Sprite = p_Sprite; }
				virtual sf::Sprite GetWidgetSprite() { return m_Sprite; }
				virtual sf::Vector2f GetWidgetPosition() { return m_WidgetPos; }
				
			protected:
				virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;
				virtual void Update() = 0;
			};
		}
	}
}