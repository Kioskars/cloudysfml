#pragma once

#include <SFML\Graphics\Text.hpp>
#include <SFML\Graphics\Font.hpp>
#include <map>
#include <vector>


namespace CE {
	namespace graphics {
		class TextManager
		{
		private:
			std::map<std::string ,sf::Font> m_FontList;
			std::vector<sf::Text> m_TextList;
		public:
			TextManager();
			~TextManager();

			void SetText(std::string p_FontName, std::string p_Text, unsigned int p_SizeText, sf::Color p_ColorText);
			sf::Text& GetText();

			void ImportFont(std::string p_Fontpath);
			sf::Font& GetFont(std::string p_FontName);
		private:
			bool FontExist(std::string p_FontName);
		};
	}
}