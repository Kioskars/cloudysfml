#include "TextManager.h"

CE::graphics::TextManager::TextManager()
{

}

CE::graphics::TextManager::~TextManager()
{
	m_FontList.clear();
	m_TextList.clear();
}

void CE::graphics::TextManager::SetText(std::string p_FontName, std::string p_Text, unsigned int p_SizeText, sf::Color p_ColorText)
{
	sf::Text xText = sf::Text();
	xText.setFont(GetFont(p_FontName));
	xText.setString(p_Text);
	xText.setCharacterSize(p_SizeText);
	xText.setColor(p_ColorText);
	m_TextList.push_back(xText);
}

sf::Text & CE::graphics::TextManager::GetText()
{
	return m_TextList.back();
}

void CE::graphics::TextManager::ImportFont(std::string p_Fontpath)
{
	std::size_t FontNamePos = p_Fontpath.find("fonts/") + 6;
	std::string FontName = p_Fontpath.substr(FontNamePos);

	if (!FontExist(FontName))
	{
		sf::Font xFont = sf::Font();
		if (!xFont.loadFromFile(p_Fontpath))
			printf("Error no Font\n");
		m_FontList.insert(std::pair<std::string, sf::Font>(FontName, xFont));
	}
	else
	{
		printf("%s already imported\n", FontName.c_str());
	}
}

sf::Font& CE::graphics::TextManager::GetFont(std::string p_FontName)
{
	if (!FontExist(p_FontName))
	{
		ImportFont("../assets/fonts/" + p_FontName);
	}
	auto it = m_FontList.find(p_FontName);

	return it->second;
}

bool CE::graphics::TextManager::FontExist(std::string p_FontName)
{
	auto it = m_FontList.find(p_FontName);
	if(it != m_FontList.end())
	{
		if (it->first == p_FontName)
		{
			printf("%s exist\n", p_FontName.c_str());
			return true;
		}
	}
	return false;
}
