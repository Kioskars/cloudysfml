#pragma once
#include <SFML\Graphics.hpp>

#include <vector>
#include <map>
#include <iostream>

namespace CE {
	namespace graphics{
		class SpriteManager
		{
		private:
			std::map<std::string, sf::Texture> m_apxTextures;
			std::map<std::string, sf::Sprite> m_apxSprites;
			std::vector<unsigned int> m_uPixelsInformation;
		public:
			SpriteManager();
			~SpriteManager();

			void CreateSprite(std::string p_sSpriteName, std::string p_sTexture, int p_iX, int p_iY, int p_iW, int p_iH);
			void CreateTexture(std::string p_sTextureName, std::string p_sFilepath, bool p_Smooth, bool p_Repeat);
			

			sf::Texture& GetTexture(std::string p_sTexture);
			sf::Sprite GetSprite(std::string p_sSprite);
			std::vector<unsigned int> GetPixelsInformation(sf::Sprite& p_Sprite);
		private:
		};
	}
}