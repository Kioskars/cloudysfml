#include "PlayState.h"
#include "../core/System.h"


CE::states::PlayState::PlayState()
{
	m_StateID = GAMESTATES_PLAY;
	m_sStateName = "PlayState";
	m_PlayerObj = nullptr;
}

CE::states::PlayState::~PlayState()
{
}

bool CE::states::PlayState::OnEnter()
{
	printf("Entering %s\n", m_sStateName.c_str());
	
	core::GlobalSystem.m_pxSystemEntityManager->CreateEntity(ENTITY_PLAYER, "HelgaEntity");
	m_PlayerObj = core::GlobalSystem.m_pxSystemEntityManager->GetHelgaEntity("HelgaEntity");
	m_PlayerObj->AssignKeyboard(core::GlobalSystem.m_pxSystemInputManager->GetKeyboard());
	m_PlayerObj->SetPosition(sf::Vector2f(25,25));
	
	core::GlobalSystem.m_pxSystemGraphicsCore->GetSpriteManager()->CreateSprite("HelgaSprite", "Sheet01", 0, 0, 64,64);
	m_PlayerObj->SetSprite(core::GlobalSystem.m_pxSystemGraphicsCore->GetSpriteManager()->GetSprite("HelgaSprite"));
	m_PlayerObj->SetPixelsInformation(core::GlobalSystem.m_pxSystemGraphicsCore->GetSpriteManager()->GetPixelsInformation(m_PlayerObj->GetSprite()));

	m_PlayerObj->GetCollider().setOutlineThickness(2);
	m_PlayerObj->GetCollider().setOutlineColor(sf::Color::Red);

	core::GlobalSystem.m_pxSystemEntityManager->CreateEntity(ENTITY_PLAYER, "TestObj");
	core::GlobalSystem.m_pxSystemGraphicsCore->GetSpriteManager()->CreateSprite("TestSprite", "Sheet01", 2, 0, 64, 64);
	m_TestObj = core::GlobalSystem.m_pxSystemEntityManager->GetHelgaEntity("TestObj");
	m_TestObj->SetPosition(sf::Vector2f(250, 125));
	m_TestObj->SetSprite(core::GlobalSystem.m_pxSystemGraphicsCore->GetSpriteManager()->GetSprite("TestSprite"));
	m_TestObj->SetPixelsInformation(core::GlobalSystem.m_pxSystemGraphicsCore->GetSpriteManager()->GetPixelsInformation(m_TestObj->GetSprite()));

	m_TestObj->GetCollider().setOutlineThickness(2);
	m_TestObj->GetCollider().setOutlineColor(sf::Color::Red);

	return true;
}

bool CE::states::PlayState::OnExit()
{
	printf("Exiting %s\n", m_sStateName.c_str());

	delete m_PlayerObj;
	m_PlayerObj = nullptr;

	delete m_TestObj;
	m_TestObj = nullptr;

	return true;
}

void CE::states::PlayState::Update()
{
	m_PlayerObj->Update();
	m_TestObj->Update();
	if (core::GlobalSystem.m_pxSystemPhysicsEngine->CheckBoundingBox(m_PlayerObj->GetCollider(), m_TestObj->GetCollider()))
	{
		m_PlayerObj->SetCollided(true);
		m_TestObj->SetCollided(true);

		m_PlayerObj->GetVertexArray()[3].color = sf::Color::Blue;
		m_PlayerObj->GetVertexArray()[2].color = sf::Color::Blue;
	}
	else
	{
		m_PlayerObj->GetVertexArray()[0].color = m_PlayerObj->GetSprite().getColor();
		m_PlayerObj->GetVertexArray()[2].color = m_PlayerObj->GetSprite().getColor();
	}
	Draw();
}

void CE::states::PlayState::Draw()
{
	core::GlobalSystem.m_pxSystemGraphicsCore->Draw(*m_TestObj);
	core::GlobalSystem.m_pxSystemGraphicsCore->Draw(*m_PlayerObj);
	core::GlobalSystem.m_pxSystemGraphicsCore->Draw(m_PlayerObj->GetCollider());
	core::GlobalSystem.m_pxSystemGraphicsCore->Draw(m_TestObj->GetCollider());

}
