#pragma once

#include <string>


enum GameStates
{
	GAMESTATES_TITLE,
	GAMESTATES_PLAY,
	GAMESTATES_MENU,
	GAMESTATES_OPTIONS,
	GAMESTATES_PAUSE,
	GAMESTATES_END
};

namespace CE {
	namespace states {
		class IGameState
		{
		protected:
			GameStates m_StateID;
			std::string m_sStateName;

			
		public:
			virtual GameStates GetGameStateID() { return m_StateID; }
			virtual std::string GetStateName() { return m_sStateName; }

			virtual bool OnEnter() = 0;
			virtual bool OnExit() = 0;

			virtual void Update() = 0;
			virtual void Draw() = 0;
		protected:
		};
	}
}