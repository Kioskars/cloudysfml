#pragma once

#include "IGameState.h"
#include "../entitites/IEntity.h"
#include "../entitites/Helga.h"
namespace CE {
	namespace states {
		class PlayState : public IGameState
		{
		private:
			entities::Helga* m_PlayerObj;
			entities::Helga* m_TestObj;
		public:
			PlayState();
			~PlayState();

			bool OnEnter();
			bool OnExit();

			void Update();
			
		private:
			void Draw();
		};
	}
}