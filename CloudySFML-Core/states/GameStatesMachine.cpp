#include "GameStatesMachine.h"

CE::states::GameStatesMachine::GameStatesMachine()
{
	SetSubsystemID(SUBSYSTEMID_GAMESTATEMACHINE);
	SetSubsystemName("GameStatesMachine");

}

CE::states::GameStatesMachine::~GameStatesMachine()
{
}

void CE::states::GameStatesMachine::ChangeGameState(IGameState* p_GameState)
{
	if (!m_apxStatesList.empty())
	{
		if (m_apxStatesList.front() == p_GameState)
		{
			return; //Do nothing
		}
		else
		{
			PopGameState();
		}
	}
	PushGameState(p_GameState);
	p_GameState->OnEnter();
}

void CE::states::GameStatesMachine::PushGameState(IGameState* p_GameState)
{
	m_apxStatesList.push_back(p_GameState);
}

void CE::states::GameStatesMachine::PopGameState()
{
	m_apxStatesList.back()->OnExit();
	m_apxStatesList.pop_back();
}

CE::states::IGameState * CE::states::GameStatesMachine::CurrentGameState()
{
	return m_apxStatesList.back();
}


bool CE::states::GameStatesMachine::Initialize()
{
	return true;
}

bool CE::states::GameStatesMachine::Shutdown()
{
	return true;
}
