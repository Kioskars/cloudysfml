#pragma once

#include "../core/ISubsystem.h"
#include "IGameState.h"

#include <vector>

namespace CE {
	namespace states {
		class GameStatesMachine : public CE::core::ISubsystem
		{
		private:
			std::vector<IGameState*> m_apxStatesList;
		public:
			GameStatesMachine();
			~GameStatesMachine();

			void ChangeGameState(IGameState* p_GameState);
			void PushGameState(IGameState* p_GameState);
			void PopGameState();

			IGameState* CurrentGameState();

			bool Initialize();
			bool Shutdown();
		private:
		};
	}
}